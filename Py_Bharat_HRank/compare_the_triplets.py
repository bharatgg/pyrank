#https://www.hackerrank.com/challenges/compare-the-triplets/problem

import os

def compare_triplets(a,b):
    alice, bob = 0,0
    for i in range(len(a)):
        if a[i]>b[i]:
            alice=alice+1
        if a[i]<b[i]:
            bob=bob+1
        if a[i]==b[i]:
            continue
    return alice, bob

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    a = list(map(int, input().rstrip().split()))
    b = list(map(int, input().rstrip().split()))
    result = compare_triplets(a, b)
    fptr.write(' '.join(map(str, result)))
    fptr.write('\n')
    fptr.close()


