#https://www.hackerrank.com/challenges/diagonal-difference/problem

import os

def diagonal_difference(arr):
    d1,d2 = 0,0
    for i in range(len(arr)):
        d1+=arr[i][i]
        d2+=arr[i][-i-1]
    return abs(d1-d2) 

if __name__ == '__main__':
    with open(os.environ['OUTPUT_PATH'], 'w') as f:
        n = int(input().strip())
        arr = []
        for _ in range(n):
          arr.append(list(map(int, input().strip().split())))

        result = diagonal_difference(arr)
        f.write(str(result) + '\n')


