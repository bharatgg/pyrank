#https://www.hackerrank.com/challenges/plus-minus/problem


import os

arr = [1,4,0,-1,-5]

def plus_minus(ar):
    total =len(arr)
    p = len([i for i in ar if i>0])
    n = len([i for i in ar if i<0])
    z = len([i for i in ar if i==0])

    return p/total, n/total, z


t = plus_minus(arr)

for i in t:
    #print("{:6f}".format(i))
    print(format(i,".6f"))
