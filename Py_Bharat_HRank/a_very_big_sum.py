
#https://www.hackerrank.com/challenges/a-very-big-sum/problem
#Author: Bharat

import os

def a_very_big_sum(ar):
    return sum(ar)

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    ar_count = int(input())
    ar = list(map(int, input().rstrip().split()))
    result = a_very_big_sum(ar)
    fptr.write(str(result) + '\n')
    fptr.close()
