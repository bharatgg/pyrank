#https://www.hackerrank.com/challenges/simple-array-sum/problem

import os

def simple_array_sum(ar):
    return sum(ar)

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    ar_count = int(input("enter the count"))
    ar = list(map(int, input().rstrip().split()))
    result = simple_array_sum(ar)
    fptr.write(str(result) + '\n')
    fptr.close()
